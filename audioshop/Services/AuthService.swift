//
//  AuthService.swift
//  audioshop
//
//  Created by Wilson Moises Diaz on 5/3/19.
//  Copyright © 2019 hipnosisclinica.cl. All rights reserved.
//

import UIKit
import Firebase

class AuthService: NSObject {
    
    static let sharedInstance = AuthService()
    private override init() { }
    
    func addAuthListener(listener: @escaping AuthStateDidChangeListenerBlock) -> AuthStateDidChangeListenerHandle {
        return Auth.auth().addStateDidChangeListener(listener)
    }
    
    func removeAuthListener(authListener: AuthStateDidChangeListenerHandle?) {
        if (authListener != nil) {
            Auth.auth().removeStateDidChangeListener(authListener!)
        }
    }
    
    func signIn(email: String, password: String, callback: AuthDataResultCallback?) {
        Auth.auth().signIn(withEmail: email, password: password, completion: callback)
    }
    
    func signUp(email: String, password: String, callback: AuthDataResultCallback?) {
        Auth.auth().createUser(withEmail: email, password: password, completion: callback)
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
        } catch {
            print("Error signing out")
        }
    }
    
    func resetPassword(email: String, callback: SendPasswordResetCallback?) {
        Auth.auth().sendPasswordReset(withEmail: email, completion: callback)
    }
    
    func updateEmail(newEmail: String, callback: UserProfileChangeCallback?) {
        Auth.auth().currentUser?.updateEmail(to: newEmail, completion: callback)
    }
    
    func updatePassword(newPassword: String, callback: UserProfileChangeCallback?) {
        Auth.auth().currentUser?.updatePassword(to: newPassword, completion: callback)
    }

}
