//
//  UIViewControllerExtension.swift
//  audioshop
//
//  Created by Wilson Moises Diaz on 5/4/19.
//  Copyright © 2019 hipnosisclinica.cl. All rights reserved.
//

import UIKit

// conjunto de alertas

extension UIViewController {
    
    func alertDialog(title: String?, message: String?, handler: (() -> Void)?) {
        
        let dialog = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let acceptButton = UIAlertAction(title: "Accept", style: .default) { (action) in
            handler?()
        }
        
        dialog.addAction(acceptButton)
        self.present(dialog, animated: true, completion: nil)
        
    }
    
}

// control de cajas de texto

extension UIViewController: UITextFieldDelegate {
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let currentTag = textField.tag
        let nextField = textField.superview?.viewWithTag(currentTag + 1) as? UITextField
        
        if (nextField == nil) {
            self.view.endEditing(true)
        } else {
            nextField?.becomeFirstResponder()
        }
        
        return true
        
    }
    
}

// control de teclado

extension UIViewController {
    
    func setupViewResizerOnKeyboardShown() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.keyboardWillShowForResizing),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.keyboardWillHideForResizing),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
    }
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let window = self.view.window?.frame {
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: window.origin.y + window.height - keyboardSize.height)
        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
        
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
        
    }
    
}
