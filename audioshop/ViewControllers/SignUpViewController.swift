//
//  SignUpViewController.swift
//  audioshop
//
//  Created by Wilson Moises Diaz on 5/4/19.
//  Copyright © 2019 hipnosisclinica.cl. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    // viewcontroller lyfecycle start
    
    override func viewDidLoad() {

        super.viewDidLoad()
        self.setupViewResizerOnKeyboardShown()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self

    }
    
    override func viewDidAppear(_ animated: Bool) {
        addAuthListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeAuthListener()
    }
    
    // viewcontroller lyfecycle end
    
    
    
    // controller specific functions start
    
    func signUp(email: String, password: String) {
        AuthService.sharedInstance.signUp(email: email, password: password) { (authDataResult, error) in
            if (error != nil) {
                self.alertDialog(title: "Error signing up", message: error?.localizedDescription, handler: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func signUp(_ sender: Any) {
        
        let email = emailTextField.text
        let password = passwordTextField.text
        let confirmPassword = confirmPasswordTextField.text
        
        if (email == nil || email == "") {
            emailTextField.becomeFirstResponder()
            self.alertDialog(title: "Error signing up", message: "Email is needed.", handler: nil)
        } else if (password == nil || password == "") {
            passwordTextField.becomeFirstResponder()
            self.alertDialog(title: "Error signing up", message: "Password is needed.", handler: nil)
        } else if (confirmPassword == nil || confirmPassword == "") {
            confirmPasswordTextField.becomeFirstResponder()
            self.alertDialog(title: "Error signing up", message: "Password confirmation is needed.", handler: nil)
        } else if (password != confirmPassword) {
            confirmPasswordTextField.becomeFirstResponder()
            self.alertDialog(title: "Error signing up", message: "Passwords need be equal", handler: nil)
        } else {
            signUp(email: email!, password: password!)
        }
        
    }
    
    // controller specific functions start
    
    
    
    // authlistener protocol implementation start
    
    var authListener: AuthStateDidChangeListenerHandle? = nil
    
    func addAuthListener() {
        self.authListener = AuthService.sharedInstance.addAuthListener(listener: { (auth, user) in
            if (user != nil) {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func removeAuthListener() {
        AuthService.sharedInstance.removeAuthListener(authListener: self.authListener)
    }
    
    // authlistener protocol implementation end
    
}
