//
//  SignInViewController.swift
//  audioshop
//
//  Created by Wilson Moises Diaz on 5/4/19.
//  Copyright © 2019 hipnosisclinica.cl. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController, AuthListenerProtocol {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // viewcontroller lyfecycle start
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupViewResizerOnKeyboardShown()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        addAuthListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeAuthListener()
    }
    
    // viewcontroller lyfecycle end
    
    
    // controller specific functions start
    
    func signIn(email: String, password: String) {
        // hacer loading
        AuthService.sharedInstance.signIn(email: email, password: password) { (authDataResult, error) in
            // finalizar loading
            
            if (error != nil) {
                self.passwordTextField.text = ""
                self.alertDialog(title: "Error signing in", message: error?.localizedDescription, handler: nil)
            }
            
        }
    }
    
    @IBAction func signIn(_ sender: Any) {
        
        let email = emailTextField.text
        let password = passwordTextField.text
        
        if (email == nil || email == "") {
            emailTextField.becomeFirstResponder()
            self.alertDialog(title: "Error signing in", message: "Email is required", handler: nil)
        } else if (password == nil || password == "") {
            passwordTextField.becomeFirstResponder()
            self.alertDialog(title: "Error signing in", message: "Password is required", handler: nil)
        } else {
            self.signIn(email: email!, password: password!)
        }
        
    }
    
    // controller specific functions start
    
    
    
    // authlistener protocol implementation start
    
    var authListener: AuthStateDidChangeListenerHandle? = nil
    
    func addAuthListener() {
        self.authListener = AuthService.sharedInstance.addAuthListener(listener: { (auth, user) in
            
        })
    }
    
    func removeAuthListener() {
        AuthService.sharedInstance.removeAuthListener(authListener: self.authListener)
    }
    
    // authlistener protocol implementation end
    
}
