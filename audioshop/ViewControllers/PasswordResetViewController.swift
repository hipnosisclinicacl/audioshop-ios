//
//  PasswordResetViewController.swift
//  audioshop
//
//  Created by Wilson Moises Diaz on 5/4/19.
//  Copyright © 2019 hipnosisclinica.cl. All rights reserved.
//

import UIKit

class PasswordResetViewController: UIViewController {
    
    let authService = AuthService.sharedInstance
    
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupViewResizerOnKeyboardShown()
        
        emailTextField.delegate = self
        
    }

    @IBAction func passwordReset(_ sender: Any) {
        
        let email = emailTextField.text
        
        if (email == nil || email == "") {
            self.emailTextField.becomeFirstResponder()
            self.alertDialog(title: "Password Reset", message: "Email is required", handler: nil)
            return
        }
        
        authService.resetPassword(email: email!) { (error) in
            if (error != nil) {
                self.alertDialog(title: "Error in password reset", message: error?.localizedDescription, handler: nil)
            } else {
                self.alertDialog(title: "Password reset instructions sent", message: "Check your email to proceed with your password reset", handler: nil)
            }
        }
        
    }
    
}
