//
//  AuthStateListenerProtocol.swift
//  audioshop
//
//  Created by Wilson Moises Diaz on 5/4/19.
//  Copyright © 2019 hipnosisclinica.cl. All rights reserved.
//

import Foundation
import Firebase

protocol AuthListenerProtocol {
    
    var authListener: AuthStateDidChangeListenerHandle? { get set }
    
    func addAuthListener()
    func removeAuthListener()
    
}
